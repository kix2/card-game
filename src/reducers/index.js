
import { combineReducers } from "redux"

import deck from "./cardReducer"

export default combineReducers({
  deck
})
