export default function reducer(state={
    players: [],
    playersNumber: 0,
    playersTable: [],
    fetching: false,
    fetched: false,
    error: null,
    roundNumber: 10,
    winnerSeted: false
  }, action) {

    switch (action.type) {
        case 'FETCH_CARD': {
            return {...state, fetching: true}
        }
        case 'FETCH_CARD_REJECTED': {
            return {...state, fetching: false, error: action.payload}
        }
        case 'SET_PLAYERS_NUMBER': {
            return {...state, playersNumber: action.payload}
        }
        case 'FETCH_CARD_FULFILLED': {
            const cards = action.payload.cards;
            const playersNumber = state.playersNumber;
            let players = [];

            for (let i = 0; i < playersNumber; i++) {
                players.push({
                    name: `player_${i + 1}`, 
                    cards: cards.splice(0, 10), 
                    sum: 0,
                    winner: false
                });
            }

            return {
                ...state,
                fetching: false,
                fetched: true,
                players: players
            }
        }
        case 'PUT_ON_TABLE': {
            return {...state, playersTable: action.payload}
        }
        case 'SET_WINNER_FLAG': {
            return {...state, winnerSeted: true}
        }
        case 'ROUND_COMPLETED': {
            return {...state, roundNumber: --state.roundNumber}
        }
        case 'SET_PLAYER_SUM': {
            return Object.assign({}, state, {
                players: state.players.map((player) => {
                    if(action.payload.name === player.name){
                        return Object.assign({}, player, {
                            sum: player.sum + action.payload.sum
                        })
                    }
                    return player;
                })
            })
        }
        case 'REMOVE_PLAYER_CARD': {
            return Object.assign({}, state, {
                players: state.players.map((player) => {
                    if(action.payload.name === player.name){
                        return Object.assign({}, player, {
                            cards: action.payload.cards
                        })
                    }
                    return player;
                })
            })
        }
        case 'SET_WINNER': {
            return Object.assign({}, state, {
                players: state.players.map((player) => {
                    if(action.payload.name === player.name){
                        return Object.assign({}, player, {
                            winner: true
                        })
                    }
                    return player;
                })
            })
        }
    }
    return state
}