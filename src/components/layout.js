import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchDeck,
         putOnTable,
         setPlayerSum,
         removePlayerCard, 
         setPlayersNumber,
         setGameWinner,
         roundCompleted,
         setWinnerFlag } from "../action/cardAction";

import PlayerCards from './player_cards';
import Board from './board';
import SetModal from './modal';

const FACE_CARDS = {
    KING  : 14,
    JACK  : 12,
    QUEEN : 13,
    ACE   : 1
}

class Layout extends Component {

    startGame(number){
        this.props.dispatch(setPlayersNumber(number));
        this.props.dispatch(fetchDeck(number));
    }

    getCardValue(val) {
        if (typeof FACE_CARDS[val] !== 'undefined') {
            return FACE_CARDS[val];
        }
        return parseInt(val, 10);
    }

    getRandomCard(cards){
        return cards[Math.floor(Math.random() * cards.length)];
    }

    getSum(cards) {
        let winner = null;
        let score = 0;
        let topCardValue = 0;

        cards.forEach(element => {
            let cardValue = this.getCardValue(element.card.value);

            if(topCardValue <= cardValue || winner == null) {
                winner = element.name;
            }

            if (topCardValue < cardValue) {
                topCardValue = cardValue;
            }
            score += cardValue;
        });

        return {
            name: winner,
            sum: score
        };
    }

    setPlayerNewCard(card, player) {
        const cards = player.cards.filter((el) => {
            return el.code !== card.code;
        });

        this.props.dispatch(removePlayerCard({cards: cards, name: player.name}));
    }

    startRound(selectedCard, player) {
        const { players } = this.props;

        this.setPlayerNewCard(selectedCard, player);

        let cards = players.filter(p => p.name !== player.name).map(p => {
            let card = this.getRandomCard(p.cards);
            this.setPlayerNewCard(card, p);
            return { card: card, name: p.name };
        });

        cards.unshift({card: selectedCard, name: player.name});

        this.props.dispatch(putOnTable(cards));
        this.props.dispatch(setPlayerSum(this.getSum(cards)));

        this.props.dispatch(roundCompleted());
    }

    setWinner(players){
        let maxVotes = Math.max(...players.map(e => e.sum));
        let winner = players.filter((index)=>{
            return maxVotes == index.sum
        });

        for(let index of winner){
            this.props.dispatch(setGameWinner(index))
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.roundNumber == 0 && !nextProps.winnerSeted){
            this.setWinner(nextProps.players)

            this.props.dispatch(setWinnerFlag())
        }
    }

    render(){
        const { playersTable, players } = this.props;

        return (
            <div>
                <div className="row top-row">
                    <div className="col-lg-2"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-8 text-center horizontal-cards">
                        <PlayerCards 
                            onCardSelect={selectedCard => this.startRound(selectedCard, players[0])}
                            player={players[0]}
                            showCard={true}
                        />
                    </div>
                    <div className="col-sm-2"></div>
                </div>
                <div className="row midle-row">
                    <div className="col-lg-2">
                        <div className="list-group">
                            <PlayerCards 
                                onCardSelect={()=>{return false}}
                                player={players[3]}
                                showCard={false}
                            />
                        </div>
                    </div>
                    <div className="players-table col-lg-8 text-center">
                        <Board playersTable={playersTable} />
                    </div>
                    <div className="col-lg-2">
                        <div className="list-group">
                            <PlayerCards 
                                onCardSelect={()=>{return false}}
                                player={players[1]}
                                showCard={false}
                            />
                        </div>
                    </div>
                </div>
                <div className="row top-row">
                    <div className="col-lg-2"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-8 text-center horizontal-cards">
                        <PlayerCards 
                            onCardSelect={()=>{return false}}
                            player={players[2]}
                            showCard={false}
                        />
                    </div>
                    <div className="col-sm-2"></div>
                </div>
                <SetModal setPlayerNumber={playerNumber => this.startGame(playerNumber)} />
            </div>
        );
    }
}

const mapStateToProps = ({
    deck: {
        playersTable,
        players,
        playersNumber,
        roundNumber,
        winnerSeted
    },
    }) => {
    return {
        playersTable,
        players,
        playersNumber,
        roundNumber,
        winnerSeted
        };
    };

export default connect(mapStateToProps)(Layout);