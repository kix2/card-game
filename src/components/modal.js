import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'react-modal-bootstrap';

class ModalExample extends React.Component {

    state = {
        modalIsOpen: true
    };

    closeModal(number) {
        this.setState({modalIsOpen: false});
        this.props.setPlayerNumber(number);
    }

    render() {
        return (
            <Modal isOpen={this.state.modalIsOpen}>
                <ModalHeader> Select number of players. </ModalHeader>
                <ModalBody>
                    <button onClick={() => this.closeModal(2)}>1</button>
                    <button onClick={() => this.closeModal(3)}>2</button>
                    <button onClick={() => this.closeModal(4)}>3</button>
                </ModalBody>
            </Modal>
        );
    }
}

export default ModalExample;