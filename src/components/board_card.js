import React from 'react';

const SingleBoardCard = (props) => {
    const { image } = props.card;

    return (
        <div className="single-card">
            <img className="list-group-item" src={image} />
            <div className="caption">
                <p>{ props.name }</p>
            </div>
        </div>
    );
};

export default SingleBoardCard;