import React from 'react';
import PlayerCard from './player_card'

const CardsList = (props) => {
    if(!props.player){
        return <span>&nbsp;</span>;
    }

    return (
        <div className="list-group list-group-horizontal">
            <div className="score">
                <span>{props.player.winner === true && <span>WiNNER iS</span> } { props.player.name } score: { props.player.sum }</span>
            </div>
            { props.player.cards.map((card)=>{
                return (
                    <PlayerCard
                        onCardSelect={props.onCardSelect}
                        key={card.code}
                        card={card}
                        showCard={props.showCard}
                    />
                );
            }) }
            
        </div>
    );
}

export default CardsList;