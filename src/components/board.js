import React from 'react';
import BoardCard from './board_card'

const Board = (props) => {
    if (!props.playersTable){
        return;
    };

    return (
        <div className="list-group list-group-horizontal">
            { props.playersTable.map(({name, card})=>{
                return (
                    <BoardCard
                        key={card.code}
                        card={card}
                        name={name}
                    />
                );
            }) }
        </div>
    );
}

export default Board;