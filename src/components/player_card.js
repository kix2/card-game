import React from 'react';

const SingleCard = ({card, onCardSelect, showCard }) => {
    const imageUrl = showCard ? card.image : './img/card_back.jpg';

    return (
        <div className="single-card player-card text-center" onClick={() => onCardSelect(card)}>
            <img  className="list-group-item" src={imageUrl} />
        </div>
    );
};

export default SingleCard;