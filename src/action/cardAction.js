import axios from "axios";

export function fetchDeck(numPlayers) {
  return function(dispatch) {
    dispatch({type: 'FETCH_CARD'});

    axios.get('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1')
      .then((response) => {
        axios.get(`https://deckofcardsapi.com/api/deck/${response.data.deck_id}/draw/?count=${numPlayers*10}`)
          .then((response) => {

            dispatch({type: 'FETCH_CARD_FULFILLED', payload: response.data})
          })
          .catch((err) => {
            dispatch({type: 'FETCH_CARD_REJECTED', payload: err})
          })
      })
      .catch((err) => {
        dispatch({type: 'FETCH_CARD_REJECTED', payload: err})
      })
  }
}

export function putOnTable(card) {
  return function(dispatch) {
    dispatch({type: 'PUT_ON_TABLE', payload: card});
  }
}

export function setPlayers(players) {
  return function(dispatch) {
    dispatch({type: 'SET_PLAYERS', payload: players});
  }
}

export function setPlayerSum(newSum) {
  return function(dispatch) {
    dispatch({type: 'SET_PLAYER_SUM', payload: newSum});
  }
}

export function removePlayerCard(newSum) {
  return function(dispatch) {
    dispatch({type: 'REMOVE_PLAYER_CARD', payload: newSum});
  }
}

export function setPlayersNumber(playersNumber) {
  return function(dispatch) {
    dispatch({type: 'SET_PLAYERS_NUMBER', payload: playersNumber});
  }
}

export function setGameWinner(winners) {
  return function(dispatch) {
    dispatch({type: 'SET_WINNER', payload: winners});
  }
}

export function roundCompleted() {
  return function(dispatch) {
    dispatch({type: 'ROUND_COMPLETED'});
  }
}

export function setWinnerFlag() {
  return function(dispatch) {
    dispatch({type: 'SET_WINNER_FLAG'});
  }
}